package com.company;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class HomeWork2 {
    private static char[][] getMatrix(int rowSize, int columnSize) {
        char[][] matrix = new char[rowSize][columnSize];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = '-';
            }
        }
        return matrix;
    }

    private static void printMatrix(char[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " ");

            }
            System.out.println(" ");
        }
    }

    public static void main(String[] args) {
        char[][] matrix = getMatrix(5, 5);

        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        int randomLine = rand.nextInt(matrix.length) + 1;
        int randomColumn = rand.nextInt(matrix.length) + 1;
        //System.out.println(randomColumn + "" + randomLine);

        System.out.println("All set. Get ready to rumble!");
        while (true) {
            printMatrix(matrix);
            System.out.println();
            try {
                System.out.print("Enter a line for fire: ");
                int line = scanner.nextInt();
                System.out.print("Enter a shooting bar: ");
                int column = scanner.nextInt();
                if ((line) == randomLine && (column) == randomColumn) {
                    matrix[line - 1][column - 1] = 'X';
                    printMatrix(matrix);
                    System.out.println("You have won!!");
                    break;
                } else {
                    matrix[line - 1][column - 1] = '*';
                }
            } catch (InputMismatchException e) {
                System.out.println("You should enter number.....");
                break;
            }
        }
    }
}

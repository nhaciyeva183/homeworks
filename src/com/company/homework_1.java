package com.company;

import java.util.Random;
import java.util.Scanner;

public class homework_1 {
    public static void main(String[] args) {
        Random rand = new Random();
        int randomNum = rand.nextInt(100);

        System.out.println("Enter a name:");
        Scanner player = new Scanner(System.in);
        String name = player.nextLine();

        System.out.println("Let the game begin!");

        int[] playerNum = new int[30];
        int count = 0;
        while (true) {
            System.out.println("Enter a number:");

            int num = player.nextInt();
            playerNum[count] = num;

            if (num == randomNum) {
                System.out.println("Congratulations," + name + "!");
                break;
            } else if (num > randomNum) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Your number is too small. Please, try again.");
            }
            count++;
        }
        System.out.println("Your numbers:");
        for (int i = 0; i <= count; i++) {
            System.out.printf("%d ", playerNum[i]);
        }

    }
}
